// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SequenceUtils.java

package sequence;

import dfa.Constants;
import java.text.DecimalFormat;

public class SequenceUtils
{

    public SequenceUtils(String seq)
    {
        this.seq = seq.toUpperCase();
    }

    public int getOccurrances(String pattern)
    {
        return seq.split(pattern).length - 1;
    }

    private int getLength()
    {
        return seq.length();
    }

    public double getFrequency(String pattern)
    {
        return roundThreeDecimals((double)getOccurrances(pattern) / ((double)getLength() * 1.0D));
    }

    public double getAliphaticIndex()
    {
        return roundThreeDecimals(100D * (1.0D * getFrequency("A") + Constants.ALIPHATIC_INDEX_A_MULTIPLIER * getFrequency("V") + Constants.ALIPHATIC_INDEX_B_MULTIPLIER * getFrequency("L") + Constants.ALIPHATIC_INDEX_B_MULTIPLIER * getFrequency("I")));
    }

    private double getCountHydrophobic()
    {
        return (double)(getOccurrances("A") + getOccurrances("F") + getOccurrances("G") + getOccurrances("I") + getOccurrances("L") + getOccurrances("M") + getOccurrances("P") + getOccurrances("V")) + getFrequency("W");
    }

    private double getCountHydrophilic()
    {
        return (double)(getOccurrances("C") + getOccurrances("N") + getOccurrances("Q") + getOccurrances("S") + getOccurrances("T") + getOccurrances("Y"));
    }

    public double getFrequencyOthers()
    {
        return roundThreeDecimals(1.0D - (getCountHydrophilic() + getCountHydrophobic()) / (1.0D * (double)getLength()));
    }

    private double roundThreeDecimals(double d)
    {
        DecimalFormat format = new DecimalFormat("#.###");
        return Double.valueOf(format.format(d)).doubleValue();
    }

    private String seq;
}
