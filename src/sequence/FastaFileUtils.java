// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   FastaFileUtils.java

package sequence;

import java.io.*;

public class FastaFileUtils
{

    public FastaFileUtils(String file)
        throws IOException
    {
        inFile = new BufferedReader(new FileReader(file));
        currentLine = inFile.readLine();
    }

    public void destroy()
        throws IOException
    {
        inFile.close();
    }

    public boolean hasNext()
        throws IOException
    {
        do
        {
            if(currentLine == null)
                return false;
            if(currentLine.length() > 0 && currentLine.charAt(0) == '>')
                return true;
            currentLine = inFile.readLine();
        } while(true);
    }

    public String getSeqName()
    {
        String split[] = currentLine.substring(1).split(" ");
        return split[0];
    }

    public String getSeq()
        throws IOException
    {
        StringBuilder seq = new StringBuilder();
        do
        {
            currentLine = inFile.readLine();
            if(currentLine == null || currentLine.length() == 0 || currentLine.charAt(0) == '>')
                return seq.toString();
            seq.append(currentLine);
        } while(true);
    }

    private BufferedReader inFile;
    private String currentLine;
}
