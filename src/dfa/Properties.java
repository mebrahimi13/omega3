package dfa;


public enum Properties
{
	Seq_Name("Seq_Name", ""),
    Freq_Of_Lysine("Freq_Of_Lysine", "K"),
    Count_Of_Lys_Phe("Count_Of_Lys_Phe", "KF"),
    Freq_Of_Gly_His("Freq_Of_Gly_His", "GH"),
    Freq_Of_Pro_Leu("Freq_Of_Pro_Leu", "PL"),
    Freq_Of_Pro_Asn("Freq_Of_Pro_Asn", "PN"),
    Aliphatic_Index("Aliphatic_Index", ""),
    Type("Type", "");
	
	private String FastaPattern;
	
	
    private Properties(String s, String FastaPattern)
    {
        this.FastaPattern = FastaPattern;
    }

    public String getFastaPattern()
    {
        return FastaPattern;
    }
}
