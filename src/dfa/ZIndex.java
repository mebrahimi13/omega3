package dfa;

import java.text.DecimalFormat;
import java.util.Map;

// Referenced classes of package dfa:
//            Constants, Properties

public class ZIndex
{

    private Map prop;
    private double zValue;
    
    public ZIndex(Map porperties)
    {
        prop = porperties;
        zValue = roundThreeDecimals(computeZValue());
    }

    public double getZValue()
    {
        return zValue;
    }

    private double computeZValue()
    {
        return Constants.Z_BASE 
        		+ Constants.Z_MULTIPLIER_ALIPHATIC_INDEX * ((Double)prop.get(Properties.Aliphatic_Index)).doubleValue() 
        		+ Constants.Z_MULTIPLIER_FREQUENCY_OF_LYSINE * ((Double)prop.get(Properties.Freq_Of_Lysine)).doubleValue() 
        		+ Constants.Z_MULTIPLIER_COUNT_OF_LYS_PHE * ((Double)prop.get(Properties.Count_Of_Lys_Phe)).doubleValue() 
        		+ Constants.Z_MULTIPLIER_FREQUENCY_OF_GLY_HIS * ((Double)prop.get(Properties.Freq_Of_Gly_His)).doubleValue() 
        		+ Constants.Z_MULTIPLIER_FREQUENCY_OF_PRO_LEU * ((Double)prop.get(Properties.Freq_Of_Pro_Leu)).doubleValue() 
        		+ Constants.Z_MULTIPLIER_FREQUENCY_OF_PRO_ASN * ((Double)prop.get(Properties.Freq_Of_Pro_Asn)).doubleValue();
    }

    public String getType()
    {
        if(Math.abs(zValue - Constants.Z_HIGH_MID) < Math.abs(zValue - Constants.Z_LOW_MID))
            return "High";
        else
            return "Low";
    }

    private double roundThreeDecimals(double d)
    {
        DecimalFormat format = new DecimalFormat("#.###");
        return Double.valueOf(format.format(d)).doubleValue();
    }

}
