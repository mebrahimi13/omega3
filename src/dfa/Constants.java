package dfa;


public abstract class Constants
{

    public Constants()
    {
    }

    public static double Z_BASE = -477;
    public static double Z_MULTIPLIER_ALIPHATIC_INDEX = 7;
    public static double Z_MULTIPLIER_FREQUENCY_OF_LYSINE = 2799;
    public static double Z_MULTIPLIER_COUNT_OF_LYS_PHE = 19;
    public static double Z_MULTIPLIER_FREQUENCY_OF_GLY_HIS = 14445;
    public static double Z_MULTIPLIER_FREQUENCY_OF_PRO_LEU = 2575;
    public static double Z_MULTIPLIER_FREQUENCY_OF_PRO_ASN = 1131;
    public static double ALIPHATIC_INDEX_A_MULTIPLIER = 2.8999999999999999D;
    public static double ALIPHATIC_INDEX_B_MULTIPLIER = 3.8999999999999999D;
    
    public static double Z_HIGH_MID = 435;
    public static double Z_LOW_MID = 483;

}
