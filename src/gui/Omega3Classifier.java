package gui;

import gui.dfa.DfaPannel;
import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;

public class Omega3Classifier
{

    public static void main(String args[])
    {
        EventQueue.invokeLater(new Runnable() {

            public void run()
            {
                try
                {
                    Omega3Classifier window = new Omega3Classifier();
                    window.frame.setVisible(true);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }

        }
);
    }

    public Omega3Classifier()
    {
        initialize();
    }

    private void initialize()
    {
        frame = new JFrame();
        frame.setMinimumSize(new Dimension(1000, 570));
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(3);
        DfaPannel dfaPannel = new DfaPannel(frame);
        JTabbedPane tabbedPane_2 = new JTabbedPane(1);
        tabbedPane_2.addTab("Discriminant Function Analysis", dfaPannel.makeDfaPanel());
        frame.getContentPane().add(tabbedPane_2, "North");
    }

    private JFrame frame;

}
