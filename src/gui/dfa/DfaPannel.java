package gui.dfa;

import dfa.Properties;
import gui.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

public class DfaPannel
{

    public DfaPannel(Frame frame)
    {
        columnNames = Properties.values();
        tableModel = new DefaultTableModel(columnNames, columnNames.length);
        this.frame = frame;
        table = getTable();
    }

    public JComponent makeDfaPanel()
    {
        JFileChooser fc = new JFileChooser();
        JPanel panel = new JPanel(false);
        panel.setLayout(new GridBagLayout());
        JScrollPane scrollPane = new JScrollPane(table);
        panel.add(scrollPane, getTableConstraints());
        JLabel infoMessageBar = new JLabel("");
        JButton clearTable = new JButton("Clear Table");
        JButton loadTable = new JButton("Load From File");
        JButton saveTable = new JButton("Write To File");
        JButton addNewRow = new JButton("Add New Row");
        JButton loadFromFastaFile = new JButton("Load From Fasta File");
        JButton doDfaAnalysis = new JButton("Start Prediction");
        clearTable.addActionListener(new ClearTableActionListener(table, infoMessageBar));
        addNewRow.addActionListener(new AddTableRowActionListener(table));
        loadTable.addActionListener(new LoadTableDataActionListener(frame, table, infoMessageBar, fc));
        saveTable.addActionListener(new WriteTableDataAction(frame, table, infoMessageBar, fc));
        loadFromFastaFile.addActionListener(new LoadFromFastaFileActionListener(frame, table, infoMessageBar, fc));
        doDfaAnalysis.addActionListener(new RunPredictionActionListener(table, columnNames));
        panel.add(infoMessageBar, getDfaInfoConstraints());
        panel.add(clearTable, getNormalConstrains(0, 2));
        panel.add(addNewRow, getNormalConstrains(1, 2));
        panel.add(loadTable, getNormalConstrains(2, 2));
        panel.add(saveTable, getNormalConstrains(3, 2));
        panel.add(loadFromFastaFile, getNormalConstrains(4, 2));
        panel.add(doDfaAnalysis, getNormalConstrains(5, 2));
        return panel;
    }

    private JTable getTable()
    {
        tableModel.setRowCount(1);
        table = new JTable(tableModel);
        table.setShowGrid(true);
        table.setGridColor(Color.BLACK);
        table.setBorder(new EtchedBorder(0));
        table.setFillsViewportHeight(true);
        TableColumnAdjuster tca = new TableColumnAdjuster(table);
        tca.adjustColumns();
        return table;
    }

    private GridBagConstraints getTableConstraints()
    {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = 2;
        c.weightx = 1.0D;
        c.weighty = 1.0D;
        c.gridwidth = 6;
        c.gridx = 0;
        c.gridy = 1;
        return c;
    }

    private GridBagConstraints getDfaInfoConstraints()
    {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = 2;
        c.gridx = 1;
        c.gridy = 0;
        c.ipady = 40;
        return c;
    }

    private GridBagConstraints getNormalConstrains(int gridx, int gridy)
    {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = gridx;
        c.gridy = gridy;
        c.anchor = 17;
        return c;
    }

    private Frame frame;
    private JTable table;
    Properties columnNames[];
    DefaultTableModel tableModel;
}
