package gui.dfa;

import dfa.Properties;
import dfa.ZIndex;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class RunPredictionActionListener
    implements ActionListener
{
	
	private JTable table;
	private Properties columnNames[];

    public RunPredictionActionListener(JTable table, Properties columnNames[])
    {
        this.table = table;
        this.columnNames = columnNames;
    }

    @SuppressWarnings("rawtypes")
	public void actionPerformed(ActionEvent arg0)
    {
        StringBuilder report = new StringBuilder();
        for(int i = 0; i < table.getModel().getRowCount(); i++)
        {
            Map input = loadInputAtRow(i, report);
            String seqName = getSeqName(i);

            if(input.size() == columnNames.length - 2)
            {
                ZIndex zIndex = new ZIndex(input);
                report.append("Seq ");
                report.append("\"" + seqName + "\":");
                report.append(" achieved Z Index value ");
                report.append("\"" + String.valueOf(zIndex.getZValue()) + "\"");
                report.append(" ==> ");
                report.append("classified as containing ");
                report.append("\"" + zIndex.getType() + "\"");
                report.append(" amount of Omega3.");
                report.append("\n");
                
                table.getModel().setValueAt(zIndex.getType(), i, columnNames.length - 1);
            } else
            {
            	report.append("Seq ");
                report.append("\"" + seqName + "\":");
                report.append(" contains missing properties and was ignored.");
                report.append("\n");
                table.getModel().setValueAt("", i, columnNames.length - 1);
            }
        }

        JTextArea textArea = new JTextArea(report.toString());
        textArea.setColumns(80);
        textArea.setRows(20);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        JOptionPane.showMessageDialog(null, new JScrollPane(textArea), "Report", 1);
    }
    
    @SuppressWarnings("rawtypes")
	private Map loadInputAtRow(int i, StringBuilder report)
    {
    	Map input = new HashMap();
        for(int j = 0; j < table.getModel().getColumnCount() - 1; j++)
        {
            Object entry = table.getModel().getValueAt(i, j);
            Properties key = columnNames[j];
            if(!key.equals(Properties.Seq_Name))
                if(entry != null && !entry.toString().equals(""))
                {
                    try
                    {
                        double value = Double.valueOf(entry.toString().trim()).doubleValue();
                        input.put(key, Double.valueOf(value));
                    }
                    catch(NumberFormatException e)
                    {
                        report.append((new StringBuilder("Invalid value is specified for ")).append(key).append(" at row ").append(i + 1).toString());
                        report.append("\n");
                    }
                } else
                {
                    report.append((new StringBuilder("Missing value for ")).append(key).append(" at row ").append(i + 1).toString());
                    report.append("\n");
                }
        }
        return input;
    }
    
    private String getSeqName(int i)
    {
    	Object entry = table.getModel().getValueAt(i, 0);
    	return String.valueOf(entry);
    }
}
