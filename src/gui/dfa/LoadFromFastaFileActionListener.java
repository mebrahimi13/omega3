package gui.dfa;

import dfa.Properties;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import sequence.FastaFileUtils;
import sequence.SequenceUtils;

public class LoadFromFastaFileActionListener
    implements ActionListener
{

    public LoadFromFastaFileActionListener(Frame frame, JTable table, JLabel dfaInfo, JFileChooser fc)
    {
        this.frame = frame;
        this.fc = fc;
        tableModel = (DefaultTableModel)table.getModel();
        this.dfaInfo = dfaInfo;
    }

    public void actionPerformed(ActionEvent arg0)
    {
        dfaInfo.setText("");
        int returnVal = fc.showOpenDialog(frame);
        if(returnVal == 0)
        {
            File file = fc.getSelectedFile();
            tableModel.setRowCount(0);
            try
            {
                FastaFileUtils seqReder;
                ArrayList row;
                for(seqReder = new FastaFileUtils(file.getAbsolutePath()); seqReder.hasNext(); tableModel.addRow(row.toArray()))
                {
                    String seqName = seqReder.getSeqName();
                    SequenceUtils seqUtil = new SequenceUtils(seqReder.getSeq());
                    row = new ArrayList();
                    Properties aproperties[];
                    int j = (aproperties = Properties.values()).length;
                    for(int i = 0; i < j; i++)
                    {
                        Properties prop = aproperties[i];
                        if(prop.equals(Properties.Seq_Name))
                            row.add(seqName);
                        else
                        if(prop.equals(Properties.Aliphatic_Index))
                            row.add(Double.valueOf(seqUtil.getAliphaticIndex()));
                        else
                        if(prop.equals(Properties.Type))
                            row.add("");
                        else
                        if(prop.equals(Properties.Count_Of_Lys_Phe))
                            row.add(Integer.valueOf(seqUtil.getOccurrances(prop.getFastaPattern())));
                        else
                            row.add(Double.valueOf(seqUtil.getFrequency(prop.getFastaPattern())));
                    }

                }

                seqReder.destroy();
            }
            catch(FileNotFoundException e)
            {
                dfaInfo.setText("File not found.");
            }
            catch(IOException e)
            {
                dfaInfo.setText((new StringBuilder("Problem encountered while loading data from file: ")).append(e.getMessage()).toString());
            }
        }
    }

    private JFileChooser fc;
    private Frame frame;
    private DefaultTableModel tableModel;
    private JLabel dfaInfo;
}
