package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class AddTableRowActionListener
    implements ActionListener
{

    public AddTableRowActionListener(JTable table)
    {
        tableModel = (DefaultTableModel)table.getModel();
    }

    public void actionPerformed(ActionEvent e)
    {
        Object rowData[] = new Object[0];
        tableModel.addRow(rowData);
    }

    private DefaultTableModel tableModel;
}
