package gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class WriteTableDataAction
    implements ActionListener
{

    public WriteTableDataAction(Frame frame, JTable table, JLabel dfaInfo, JFileChooser fc)
    {
        this.frame = frame;
        this.fc = fc;
        tableModel = (DefaultTableModel)table.getModel();
        this.dfaInfo = dfaInfo;
    }

    public void actionPerformed(ActionEvent e)
    {
        dfaInfo.setText("");
        int returnVal = fc.showSaveDialog(frame);
        if(returnVal == 0)
        {
            File file = fc.getSelectedFile();
            try
            {
                FileWriter out = new FileWriter(file);
                for(int i = 0; i < tableModel.getRowCount(); i++)
                {
                    for(int j = 0; j < tableModel.getColumnCount(); j++)
                    {
                        Object entry = tableModel.getValueAt(i, j);
                        entry = entry != null ? ((Object) (entry.toString())) : "";
                        out.write((new StringBuilder(String.valueOf((String)entry))).append(",").toString());
                    }

                    out.write("\n");
                }

                out.close();
            }
            catch(IOException e1)
            {
                dfaInfo.setText((new StringBuilder("Problem encountered while writing data to file: ")).append(e1.getMessage()).toString());
            }
        }
    }

    private JFileChooser fc;
    private Frame frame;
    private DefaultTableModel tableModel;
    private JLabel dfaInfo;
}
